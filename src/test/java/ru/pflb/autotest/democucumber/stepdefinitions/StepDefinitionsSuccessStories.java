package ru.pflb.autotest.democucumber.stepdefinitions;

import cucumber.api.java.ru.Дано;
import cucumber.api.java.ru.Если;
import cucumber.api.java.ru.И;
import cucumber.api.java.ru.Тогда;
import ru.pflb.autotest.democucumber.pages.SuccessStoriesPage;

public class StepDefinitionsSuccessStories {
    SuccessStoriesPage successStoriesPage = new SuccessStoriesPage();

    @Дано("открыта страница успешных историй javarush.ru.groups.stories")
    public void открыта_страница_успешных_историй_javarush_ru_groups_stories() {
        successStoriesPage.openPage();
    }

    @Если("истории отсортированы по популярности")
    public void истории_отсортированы_по_популярности() {
        successStoriesPage.currentSortStories();
    }

    @Если("сделана сортировка 'старые'")
    public void сделана_сортировка_старые_() {
        successStoriesPage.sortOldStories();
    }

    @Если("открыта 2я страница с иториями")
    public void открыта_2я_страница_с_иториями() {
        successStoriesPage.openTwoPageWithStory();
    }

    @Если("показаны ещё 12 историй")
    public void показаны_ещё_12_историй() {
        successStoriesPage.openYet12Story();

    }

    @Тогда("сделать сортировку 'новые'")
    public void сделать_сортировку_новые_() {
        successStoriesPage.sortNewStories();
    }

    @Тогда("открыть 13ю историю")
    public void открыть_13ю_историю() {

    }

    @И("открыть самую новую историю")
    public void открыть_самую_новую_историю() {
        successStoriesPage.openNewStory();
        successStoriesPage.close();
    }
}
