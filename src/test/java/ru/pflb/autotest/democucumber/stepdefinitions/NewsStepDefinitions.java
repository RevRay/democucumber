package ru.pflb.autotest.democucumber.stepdefinitions;

import cucumber.api.java.ru.Дано;
import cucumber.api.java.ru.Если;
import cucumber.api.java.ru.Тогда;
import org.testng.Assert;
import ru.pflb.autotest.democucumber.pages.JavaRushNewsPage;

public class NewsStepDefinitions {
    JavaRushNewsPage newsPage = new JavaRushNewsPage();

    @Дано("открыта страница с активностями")
    public void открыта_страница_с_активностями() {
        newsPage.open();
    }

    @Если("открыть меню выбора города")
    public void открыть_меню_выбора_города() {
        newsPage.openCityMenu();
    }

    @Если("выбрать город {string}")
    public void выбрать_город(String city) {
        newsPage.chooseCity(city);
    }

    @Тогда("все показанные карточки - из города {string}")
    public void все_показанные_карточки_из_города(String city) {
        Assert.assertTrue(newsPage.areAllCardsFromTheSamePlace(city));
    }
}
