package ru.pflb.autotest.democucumber.stepdefinitions;

import cucumber.api.java.ru.Дано;
import cucumber.api.java.ru.Если;
import cucumber.api.java.ru.Тогда;
import ru.pflb.autotest.democucumber.pages.PricesPage;

public class PricesPageDefinition {
    PricesPage pricesPage = new PricesPage();

    @Дано("открыта страница с подписками")
    public void открыта_страница_с_подписками() {
        pricesPage.open();
    }

    @Если("нажата кнопка дневной ночной режим")
    public void нажата_кнопка_дневной_ночной_режим_switcher_daynight() {
        pricesPage.checkDayNight();
    }

    @Если("нажата кнопка подписки")
    public void нажата_кнопка_подписки_prices_buy_key_JR__PREMIUM() {
        pricesPage.clickOnPremiumButton();
    }

    @Тогда("проверяем наличие надписи вход")
    public void проверяем_наличие_надписи_вход_auth_widget_Вход() {
        pricesPage.checkForEnterText();
    }
}
