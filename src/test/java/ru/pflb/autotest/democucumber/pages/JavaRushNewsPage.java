package ru.pflb.autotest.democucumber.pages;

public class JavaRushNewsPage extends BasePage {
    private static String NEWS_PAGE_URL = "https://javarush.ru/news";

    private static String OPEN_CITY_FILTER_MENU_XPATH = "//p-dropdown[@formcontrolname='city']";
    private static String OPEN_ACTIVITY_FILTER_MENU_XPATH = "//p-dropdown[@formcontrolname='type']";

    private static String WRAPPER_SPAN_ELEMENT_XPATH_FORMAT = "//div[contains(@class, 'ui-dropdown-items-wrapper')]//ul/li/span[text()='%s']";
    private static String CITY_FILTER_MENU_XPATH_FORMAT = OPEN_CITY_FILTER_MENU_XPATH + WRAPPER_SPAN_ELEMENT_XPATH_FORMAT;
    private static String ACTIVITY_FILTER_MENU_XPATH_FORMAT = OPEN_ACTIVITY_FILTER_MENU_XPATH + WRAPPER_SPAN_ELEMENT_XPATH_FORMAT;

    private static String NEWS_CARD_XPATH_FORMAT = "(//jr-news-card)[%d]";
    private static String NEWS_CARD_CITY_XPATH_FORMAT = NEWS_CARD_XPATH_FORMAT
            + "/div[contains(@class,'news-card')]//span[contains(@class,'news-card__location')][text()='%s']";
    private static int MIN_CARD_INDEX = 1;
    private static int MAX_CARD_INDEX = 12;


    public void open() {
        driver.get(NEWS_PAGE_URL);
    }

    public void openCityMenu() {
        driver.findElementByXPath(OPEN_CITY_FILTER_MENU_XPATH).click();
    }

    public void chooseCity(String cityName) {
        if ( null == cityName ) {
            throw new IllegalArgumentException("Non-null name of city is required");
        }
        if ( cityName.trim().isEmpty() ) {
            throw new IllegalArgumentException("Non-blank name of city is required");
        }
        driver.findElementByXPath(
                String.format(CITY_FILTER_MENU_XPATH_FORMAT, cityName)
        ).click();
    }

    public void openActivityMenu() {
        driver.findElementByXPath(OPEN_ACTIVITY_FILTER_MENU_XPATH).click();
    }

    public void chooseActivity(String activityName) {
        if ( null == activityName ) {
            throw new IllegalArgumentException("Non-null name of activity is required");
        }
        if ( activityName.trim().isEmpty() ) {
            throw new IllegalArgumentException("Non-blank name of activity is required");
        }
        driver.findElementByXPath(
                String.format(ACTIVITY_FILTER_MENU_XPATH_FORMAT, activityName)
        ).click();
    }

    // Supposing it's 1-st page (i.e. there are always MAX_CARD_INDEX cards)
    public boolean areAllCardsFromTheSamePlace(String city) {
        if ( null == city ) {
            throw new IllegalArgumentException("Non-null name of city is required");
        }
        if ( city.trim().isEmpty() ) {
            throw new IllegalArgumentException("Non-blank name of city is required");
        }
        for ( int cardNumber = MIN_CARD_INDEX; cardNumber <= MAX_CARD_INDEX; cardNumber++ ) {
            String cardCity = driver.findElementByXPath(
                    String.format(NEWS_CARD_CITY_XPATH_FORMAT, cardNumber, city)
            ).getText();

            if ( !city.equals(cardCity) ) {
                return false;
            }
        }

        return true;
    }
}
