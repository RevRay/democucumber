package ru.pflb.autotest.democucumber.pages;

import org.openqa.selenium.WebElement;
import ru.pflb.autotest.democucumber.pages.BasePage;

public class SuccessStoriesPage extends BasePage {
    public void openPage(){
        driver.get("https://javarush.ru/groups/stories");
    }

    public void currentSortStories(){
        String sortLinkXpath = "//li[@class='tabs__item tabs__item--active']/span";
        WebElement sortLink = driver.findElementByXPath(sortLinkXpath);
        //sortLink.click();
    }

    public void sortNewStories(){
        String sortLinkXpath = "//span[@class='tabs__button' and contains(text(), 'новые')]";
        WebElement sortLink = driver.findElementByXPath(sortLinkXpath);
        sortLink.click();
    }

    public void sortOldStories(){
        String sortLinkXpath = "//span[@class='tabs__button' and contains(text(), 'старые')]";
        WebElement sortLink = driver.findElementByXPath(sortLinkXpath);
        sortLink.click();
    }

    public void openNewStory(){
        String storyXpath = "//div[@class='post-card__content']/a";
        WebElement newStoryLink = driver.findElementByXPath(storyXpath);
        newStoryLink.click();
    }

    public void openTwoPageWithStory(){
        String pageXpath = "//a[@class='pagination__link' and contains(text(), '2')]";
        WebElement twoPage = driver.findElementByXPath(pageXpath);
        driver.scrollToElement(twoPage);
        twoPage.click();
    }

    public void close(){
        driver.close();
    }

    public void openYet12Story() {
        String yet12StoryXpath = "//button[@class='button button--more button--md']";
        WebElement yet12Story = driver.findElementByXPath(yet12StoryXpath);
        driver.scrollToElement(yet12Story);
        yet12Story.click();
    }
}
